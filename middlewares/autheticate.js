const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Viewer = require('../models/Viewer');

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const viewer = await Viewer.findOne({
      email,
    })
      .lean()
      .exec();

    if (!viewer) {
      req.error = { message: 'Email not found', viewer };
      next();
      return;
    }

    const passwordsMatched = bcrypt.compareSync(password, viewer.password);
    if (!passwordsMatched) {
      req.error = { message: 'Wrong password', passwordsMatched };
      next();
      return;
    }

    const token = jwt.sign(
      {
        viewerId: viewer.id,
        email: viewer.email,
      },
      process.env.TOKEN,
      { expiresIn: '2 days' }
    );

    res.json({
      message: 'Log-in successfully',
      token,
    });
  } catch (error) {
    req.error = error;
    next();
  }
};

exports.authenticate = (req, res, next) => {
  try {
    const authHeader = req.get('Authorization');
    if (!authHeader) {
      req.error = { message: 'Log-in again.' };
      next();
    }

    const token = authHeader.split(' ')[1];
    let decodedToken;
    decodedToken = jwt.verify(token, process.env.TOKEN);

    if (!decodedToken) {
      req.error = { message: 'Cannot authenticate.' };
      next();
    }

    res.json({
      message: 'aunthenticated',
      token,
    });
  } catch (error) {
    req.error = error;
    next();
  }
};

// TODO
exports.logout = async (req, res, next) => {
  try {
    const { email } = req.body;

    const viewer = await Viewer.findOne({
      email,
    })
      .lean()
      .exec();

    if (!viewer) {
      req.error = { message: 'Error logging out', viewer };
      next();
      return;
    }

    res.json({
      message: 'You are logged out.',
    });
  } catch (error) {
    next(error);
  }
};
