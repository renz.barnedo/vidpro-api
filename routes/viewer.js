const express = require('express');
const router = express.Router();

const { addViewer } = require('../controllers/viewer');
const { login, authenticate } = require('../middlewares/autheticate');

router.post('/new', addViewer);
router.post('/login', login);
router.post('/authenticate', authenticate);

module.exports = router;
