const moment = require('moment-timezone');
const generator = require('generate-password');
const bcrypt = require('bcryptjs');

const Viewer = require('../models/Viewer');

const SALT_ROUNDS = 12;
const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A';
const TIMEZONE = 'Asia/Manila';

exports.addViewer = async (req, res, next) => {
  try {
    let { email, salesPagesLinks } = req.body;

    const existingEmail = await Viewer.findOne({ email }, 'email')
      .lean()
      .exec();
    if (existingEmail) {
      req.error = { message: 'email exists', existingEmail };
      next();
      return;
    }

    let password = generator.generate({
      length: 10,
      numbers: true,
      uppercase: false,
      strict: true,
    });
    const textpass = password;
    password = bcrypt.hashSync(password, SALT_ROUNDS);

    const request = {
      email,
      password,
      salesPagesLinks,
      datetime: moment().tz(TIMEZONE).format(DATETIME_FORMAT),
      isDeleted: false,
    };

    const viewer = new Viewer(request);
    const result = await viewer.save(viewer);

    if (!result) {
      req.error = result;
      next();
    }

    res.json({
      message: 'created',
      account: {
        email: email,
        password: textpass,
      },
      salespages: salesPagesLinks,
      datetime: request.datetime,
      isDeleted: request.isDeleted,
    });
  } catch (error) {
    req.error = error;
    next();
  }
};
