const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const viewerSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  salesPagesLinks: {
    type: Array,
    required: true,
  },
  datetime: {
    type: Date,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  },
});

module.exports = mongoose.model('Viewer', viewerSchema);
