const express = require('express');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const mongoose = require('mongoose');

const viewerRoutes = require('./routes/viewer');

require('dotenv').config();

const app = express();

let corsConfig;
if (process.env.NODE_ENV === 'production') {
  const allowedDomains = [
    'https://vidpro.tech/',
    'https://vidpro.netlify.app/',
  ];

  corsConfig = {
    origin: (origin, callback) => {
      if (!origin) {
        return callback(null, true);
      }

      if (allowedDomains.indexOf(origin) === -1) {
        const msg = `This site ${origin} does not have an access. Only specific domains are allowed to access it.`;
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  };
}

app.use(cors(corsConfig));

app.use(helmet());
app.use(compression({ level: 9 }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/viewer', viewerRoutes);

app.use(
  '/assets',
  express.static(__dirname + '/' + process.env.SOURCES_FOLDER)
);

app.use((req, res) => {
  if (!req.error) {
    return;
  }
  console.log('----------------ERROR MIDDLEWARE----------------');
  console.log(req.error);
  console.log('----------------ERROR MIDDLEWARE----------------');
  res.status(500).json({
    message: req.error,
  });
});

const PORT = process.env.PORT || 5000;
const MESSAGE = `Server CONNECTED running on ${process.env.NODE_ENV} mode port on ${PORT}`;

mongoose.connect(
  process.env.MONGO_DB_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error) => {
    if (error) {
      console.log('*****Mongo connection error:', error);
      return;
    }
    app.listen(PORT, console.log(MESSAGE));
  }
);
